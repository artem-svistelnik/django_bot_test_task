from django.contrib import admin
from .models import ReferralId, TelegramUser
# Register your models here.
admin.site.register(ReferralId)
admin.site.register(TelegramUser)