from django.shortcuts import render

# Create your views here.
import json
import os

import requests
from django.http import JsonResponse
from django.views import View
from .models import TelegramUser,ReferralId

import yaml
with open('config.yaml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)
TELEGRAM_URL=config["TELEGRAM_URL"]
BOT_TOKEN=config["BOT_TOKEN"]


class TutorialBotView(View):
    def post(self, request, *args, **kwargs):
        t_data = json.loads(request.body)
        print(t_data)
        try:
            referral_id=t_data['message']['text'].lstrip('/start ')
            if not referral_id.isdigit():
                self.send_message(
                   message= 'Вы не можете начать использовать бот без реферальной ссылки. Перейдите в бот с помощью реферальной ссылки',
                    chat_id=t_data['message']['chat']['id'])
                return JsonResponse({"ok": "POST request processed"})
            try:
                ref = ReferralId.objects.get(referral_id=int(referral_id))
            except ReferralId.DoesNotExist:
                self.send_message(message='Не действительная ссылка', chat_id=t_data['message']['chat']['id'])
                return JsonResponse({"ok": "POST request processed"})

            try:
                user = TelegramUser.objects.get(username=t_data['message']['chat']['username'])
                self.send_message(message=f'Приветствую {user.username} \nТвой уникальный код  {user.id}',
                                  chat_id=t_data['message']['chat']['id'])
                return JsonResponse({"ok": "POST request processed"})
            except TelegramUser.DoesNotExist:
                user = TelegramUser.objects.create(
                    referral_id=ref,
                    username=t_data['message']['chat']['username']
                )
                user.save()
                self.send_message(message=f'Приветствую {user.username} \nТвой уникальный код  {user.id}',
                                 chat_id=t_data['message']['chat']['id'])
                return JsonResponse({"ok": "POST request processed"})

        except KeyError as K:
            self.send_message(
                message='Вы не можете начать использовать бот без реферальной ссылки. Перейдите в бот с помощью реферальной ссылки',
                chat_id=t_data['my_chat_member']['chat']['id'])
            return JsonResponse({"ok": "POST request processed"})
        except IndexError:
            self.send_message(message='Вы не можете начать использовать бот без реферальной ссылки. Перейдите в бот с помощью реферальной ссылки',
                              chat_id=t_data['message']['chat']['id'])
            return JsonResponse({"ok": "POST request processed"})
        except Exception as E:
            return JsonResponse({"ok": "POST request processed"})



    @staticmethod
    def send_message(message, chat_id):
        data = {
            "chat_id": chat_id,
            "text": message,
            # "parse_mode": "Markdown",
        }
        response = requests.post(
            f"{TELEGRAM_URL}{BOT_TOKEN}/sendMessage", data=data
        )
        print('response: ',response)