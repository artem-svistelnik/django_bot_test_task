from django.db import models
from django.core.validators import RegexValidator

REFERRAL_ID_VALIDATOR = RegexValidator(r'[0-9]{8}', 'only 8 positive nums')


class ReferralId(models.Model):
    referral_id = models.CharField(validators=[REFERRAL_ID_VALIDATOR],unique=True,max_length=8)

    def __str__(self):
        return str(self.referral_id)


# Create your models here.
class TelegramUser(models.Model):
    referral_id = models.ForeignKey(ReferralId, on_delete=models.CASCADE,blank=False)
    username = models.CharField(max_length=250)

    def __str__(self):
        return f'{self.username}#id : {self.id}'
