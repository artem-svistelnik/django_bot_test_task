
# Django bot test task

**Deployment and launch instructions**

You must install ngrok:
`sudo apt update`

`sudo apt install snapd`

`sudo snap install ngrok`


Launch ngrok in new tirminal
`ngrok http`
_You must add a port number at the end of the line. For example: "5000 or 8000"_

In an empty directory, execute the command::

`git clone https://gitlab.com/artem-svistelnik/django_bot_test_task.git`

Create virtual environment (venv) and run it

`python3 -m venv venv`

`source venv/bin/activate`

Go to the directory with the project

`cd django_bot_project`

Install all required dependencies

`pip install -r requirements.txt `

You must create postgresql database
You must create config.yaml file and add config data like config_example.yaml


Create migration

`python manage.py makemigrations`

Apply migration

`python manage.py migrate`

Create superuser

`python manage.py createsuperuser`

run server

`python manage.py runserver `
_You can add a port number at the end of the line. For example: "5000"_


**Next, you need to fill the base with several entries in the "ReferralId" table**


**Description of requests :**

1.setup webhook:
 post requests on  https://api.telegram.org/bot<bot_token>/setWebhook?url=<ngrok_url>/webhooks/tutorial/

example:

 https://api.telegram.org/bot5046417605:asfasgasgassaasg/setWebhook?url=https://4e53-37-57-170-94.ngrok.io/webhooks/tutorial/
